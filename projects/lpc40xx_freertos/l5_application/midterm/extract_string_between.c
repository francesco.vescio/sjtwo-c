#include "extract_string_between.h"

bool extract_string_between(const char *source, char *output_string, char between) {
  bool between_found = false;
  if (!source || !output_string || !between) {
    return between_found;
  }
  if (strlen(source) < 1 /*or between doesn't show up at least twice*/) {
    return between_found;
  }
  char *left;
  char *right;

  left = strstr(source, between);
  if (left != NULL) {
    right = strstr(source, between);
    if (right != NULL) {
      output_string = right - left - strlen(between);
      between_found = true;
    }
  }
  return between_found;
}