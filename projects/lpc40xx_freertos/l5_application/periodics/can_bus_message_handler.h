#pragma once

#include <stdint.h>

void can_bus_message_handler__switch_led_logic_init(void);
void can_bus_message_handler__send_message(uint64_t message);
void can_bus_message_handler__receive_message(void);
void can_bus_message_handler__switch_logic_run_once(void);
