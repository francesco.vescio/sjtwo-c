#include <stdlib.h>

#include "can_bus.h"
#include "can_bus_initializer.h"

void can_bus_initializer__init_can_1(void) {
  can__init(can1, 100, 500, 500, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);
}

void can_bus_initializer__handle_bus_off(void) {
  if (can__is_bus_off(can1)) {
    can__reset_bus(can1);
  }
}
