#include <stdlib.h>

#include "unity.h"

#include "Mockcan_bus.h"
#include "can_bus_initializer.h"

void setUp(void) {}

void tearDown(void) {}

void test__can_bus_initializer__init_can_1(void) {
  can__init_ExpectAnyArgsAndReturn(NULL);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_ExpectAnyArgs();

  can_bus_initializer__init_can_1();
}

void test__can_bus_initializer__handle_bus_off(void) {
  bool ret;
  can__is_bus_off_ExpectAnyArgsAndReturn(ret);
  if (ret) {
    can__reset_bus_ExpectAnyArgs();
  }

  can_bus_initializer__handle_bus_off();
}