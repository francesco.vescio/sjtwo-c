#include <stdio.h>

#include "unity.h"

#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "can_bus_message_handler.h"

void setUp(void) {}

void tearDown(void) {}

void test__can_bus_message_handler__switch_led_logic_init(void) {
  gpio_s gpio_led2 = {};
  gpio_s gpio_switch0 = {};

  gpio__construct_as_output_ExpectAnyArgsAndReturn(gpio_led2);
  gpio__construct_as_input_ExpectAnyArgsAndReturn(gpio_switch0);
  gpio__reset_ExpectAnyArgs();

  can_bus_message_handler__switch_led_logic_init();
}

void test__can_bus_message_handler__send_message(void) {
  can__tx_ExpectAnyArgsAndReturn(NULL);

  can_bus_message_handler__send_message(0x00);
}

void test__can_bus_message_handler__switch_logic_run_once(void) {
  bool ret;
  gpio__get_ExpectAnyArgsAndReturn(ret);
  can__tx_ExpectAnyArgsAndReturn(ret);

  can_bus_message_handler__switch_logic_run_once();
}