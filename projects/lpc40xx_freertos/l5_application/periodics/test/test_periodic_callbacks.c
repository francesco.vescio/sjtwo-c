#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockcan_bus_initializer.h"
#include "Mockcan_bus_message_handler.h"
#include "Mockswitch_led_logic.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  switch_led_logic__initialize_Expect();
  can_bus_initializer__init_can_1_Expect();
  can_bus_message_handler__switch_led_logic_init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  switch_led_logic__run_once_Expect();
  can_bus_initializer__handle_bus_off_Expect();
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  // gpio_s gpio = {};
  // board_io__get_led1_ExpectAndReturn(gpio);
  // gpio__toggle_Expect(gpio);

  can_bus_message_handler__switch_logic_run_once_Expect();
  // can_bus_message_handler__receive_message_Expect();

  periodic_callbacks__10Hz(0);
}