#include "can_bus_message_handler.h"
#include "can_bus.h"
#include "gpio.h"

static uint32_t tx_msg_id = 0x123;
static gpio_s led2;
static gpio_s switch2;
static can__msg_t rx_msg = {};
static can__msg_t tx_msg = {};

void can_bus_message_handler__switch_led_logic_init(void) {
  led2 = gpio__construct_as_output(GPIO__PORT_1, 26);
  switch2 = gpio__construct_as_input(GPIO__PORT_0, 29);
  gpio__reset(led2);
}

void can_bus_message_handler__send_message(uint64_t message) {
  tx_msg.msg_id = tx_msg_id;
  tx_msg_id++;
  tx_msg.frame_fields.is_29bit = 0;
  tx_msg.frame_fields.data_len = 1;
  tx_msg.data.qword = message;
  can__tx(can1, &tx_msg, 0);
}

void can_bus_message_handler__receive_message(void) {
  while (can__rx(can1, &rx_msg, 0)) {
    if (rx_msg.data.qword == 0xAA) {
      gpio__set(led2);
    } else {
      gpio__reset(led2);
    }
  }
}

void can_bus_message_handler__switch_logic_run_once(void) {
  if (gpio__get(switch2)) {
    can_bus_message_handler__send_message(0xAA);
  } else {
    can_bus_message_handler__send_message(0x11);
  }
}
